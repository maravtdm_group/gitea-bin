CONF_EXISTS=$(grep -c "program:gitea" /etc/supervisord.conf)

if [ $CONF_EXISTS -eq "1" ]; then
  if [ -f /etc/supervisord.conf.orig ]; then
    cat /etc/supervisord.conf.orig > /etc/supervisord.conf
  else
    echo "Unable to restore original configuration"
    echo "See /etc/supervisord.conf for details / modifications"
  fi
fi

echo ""
echo "Gitea successfully removed"
echo "You may also need to manually remove gitea user/group"
echo "# userdel -r gitea"
echo ""
