CONF_EXISTS=$(grep -c "program:gitea" /etc/supervisord.conf)

if [ $CONF_EXISTS -ne "1" ]; then
  if [ -w /etc/supervisord.conf ]; then
    if [ ! -f /etc/supervisord.conf.orig ]; then
      cp /etc/supervisord.conf /etc/supervisord.conf.orig
    fi
    cat /var/lib/gitea/gitea-supervisor.conf >> /etc/supervisord.conf
  fi
fi
